/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorserver;

import java.io.PrintWriter;

/**
 *
 * @author Livin
 */
public class ClientsObjectsContainer {
    private PrintWriter clientSenderObjetNode1 = null;
    private PrintWriter clientSenderObjetNode2 = null;
    private PrintWriter clientSenderObjetNode3 = null;
    private PrintWriter clientSenderObjetNode4 = null;
    
    public void setClientSenderObjet(PrintWriter clientSenderObjet, String nodeName) {
        if (nodeName.equals("Node 1")) {
            this.clientSenderObjetNode1 = clientSenderObjet;
        } else if (nodeName.equals("Node 2")) {
            this.clientSenderObjetNode2 = clientSenderObjet;
        } else if (nodeName.equals("Node 3")) {
            this.clientSenderObjetNode3 = clientSenderObjet;
        } else if (nodeName.equals("Node 4")) {
            this.clientSenderObjetNode4 = clientSenderObjet;
        }
    }
    
    public PrintWriter getClientSenderObjet(String nodeName) {
        if (nodeName.equals("NODE1")) {
            return clientSenderObjetNode1;
        } else if (nodeName.equals("NODE2")) {
            return clientSenderObjetNode1;
        } else if (nodeName.equals("NODE3")) {
            return clientSenderObjetNode1;
        } else if (nodeName.equals("NODE4")) {
            return clientSenderObjetNode1;
        } else {
            return null;
        }
    }
    
    public void sendMessageToClients(String messge) {
        if (clientSenderObjetNode1 != null) {
            clientSenderObjetNode1.println(messge);
        }
        if (clientSenderObjetNode2 != null) {
            clientSenderObjetNode2.println(messge);
        }
        if (clientSenderObjetNode3 != null) {
            clientSenderObjetNode3.println(messge);
        }
        if (clientSenderObjetNode4 != null) {
            clientSenderObjetNode4.println(messge);
        }
    }
    
}
