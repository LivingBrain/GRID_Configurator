/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorserver;

/**
 *
 * @author Livin
 */
public class ObjectContainer {
    private Server storedObject;

    public void setObject(Server storedObject) {
        this.storedObject = storedObject;
    }
    
    public Server getObject() {
        if (storedObject != null) {
            return storedObject;
        } else {
            return null;
        }
    }
}
