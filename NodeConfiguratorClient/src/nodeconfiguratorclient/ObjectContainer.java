/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient;

/**
 *
 * @author Livin
 */
public class ObjectContainer {
    private Client storedObject;

    public void setObject(Client storedObject) {
        this.storedObject = storedObject;
    }
    
    public Client getObject() {
        if (storedObject != null) {
            return storedObject;
        } else {
            return null;
        }
    }
}
