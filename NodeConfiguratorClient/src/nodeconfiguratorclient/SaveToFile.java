/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import nodeconfiguratorclient.data.RunTimes;

/**
 *
 * @author Livin
 */
public class SaveToFile {
    
    public void etcHosts(File file, String runTimeNumber) throws FileNotFoundException, IOException {
        BuildIPsAndDomains buildIPsAndDomains = new BuildIPsAndDomains();
//        File file = new File("C:\\Windows\\System32\\drivers\\etc\\hosts");
        if (file.exists()) {
            FileWriter fileWriter = new FileWriter(file);
            if (runTimeNumber.equals("RS590")) {
                fileWriter.write(buildIPsAndDomains.getConcatenatedString(RunTimes.RS590.getIP()));
            } else if (runTimeNumber.equals("RS591")) {
                fileWriter.write(buildIPsAndDomains.getConcatenatedString(RunTimes.RS591.getIP()));
            } else if (runTimeNumber.equals("JN100")) {
                fileWriter.write(buildIPsAndDomains.getConcatenatedString(RunTimes.JN100.getIP()));
            } else if (runTimeNumber.equals("JN101")) {
                fileWriter.write(buildIPsAndDomains.getConcatenatedString(RunTimes.JN101.getIP()));
            } else if (runTimeNumber.equals("JNLB")) {
                fileWriter.write(RunTimes.JNLB.getIP());
            } else if (runTimeNumber.equals("RSLB")) {
                fileWriter.write(RunTimes.RSLB.getIP());
            } else if (runTimeNumber.equals("Default")) {
                fileWriter.write(RunTimes.DEFAULT.getIP());
            }
            fileWriter.flush();
            fileWriter.close();
        }
    }
    
    
}
