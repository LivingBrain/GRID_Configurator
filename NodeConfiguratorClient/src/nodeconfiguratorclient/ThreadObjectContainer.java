/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient;

/**
 *
 * @author Livin
 */
public class ThreadObjectContainer {
    
    private Thread storedObject;

    public void setThreadObject(Thread thread) {
        this.storedObject = thread;
    }
    
    public Thread getThreadObject() {
        if (storedObject != null) {
            return storedObject;
        } else {
            return null;
        }
    }
    
    
}
