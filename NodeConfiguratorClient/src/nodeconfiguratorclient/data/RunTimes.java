/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient.data;

/**
 *
 * @author Livin
 */
public enum RunTimes {
    RS590("10.20.18.33"),
    RS591("10.20.18.34"),
    JN100("10.20.16.129"),
    JN101("10.20.16.130"),
    JNLB("37.110.194.241 engie-electrabel.be www.engie-electrabel.be \r\n"
            + "37.110.194.241 electrabel.be www.electrabel.be \r\n"
            + "37.110.194.240 electrabel.com www.electrabel.com \r\n"
            + "37.110.194.238 gdfsuez-energie.de www.gdfsuez-energie.de"),
    RSLB("94.236.33.55 engie-electrabel.be www.engie-electrabel.be \r\n"
            + "94.236.33.55 electrabel.be www.electrabel.be \r\n"
            + "46.38.177.139 electrabel.com www.electrabel.com \r\n"
            + "94.236.33.51 gdfsuez-energie.de www.gdfsuez-energie.de"),
    DEFAULT("# Copyright (c) 1993-2009 Microsoft Corp. \r\n"
            + "# \r\n"
            + "# This file contains the mappings of IP addresses to host names. Each \r\n"
            + "# entry should be kept on an individual line. The IP address should \r\n"
            + "# be placed in the first column followed by the corresponding host name. \r\n"
            + "# The IP address and the host name should be separated by at least one \r\n"
            + "# space. \r\n"
            + "# \r\n"
            + "# Additionally, comments (such as these) may be inserted on individual \r\n"
            + "# lines or following the machine name denoted by a '#' symbol. \r\n"
            + "# \r\n"
            + "# For example: \r\n"
            + "# \r\n"
            + "#      102.54.94.97     rhino.acme.com          # source server \r\n"
            + "#       38.25.63.10     x.acme.com              # x client host \r\n"
            + "# \r\n"
            + "# localhost name resolution is handled within DNS itself. \r\n"
            + "#   127.0.0.1       localhost \r\n"
            + "#   ::1             localhost");
    
    private String runTimeIPs;
    
    RunTimes(String runTimeIPs) {
        this.runTimeIPs = runTimeIPs;
    }
    
    public String getIP() {
        return runTimeIPs;
    }
}
