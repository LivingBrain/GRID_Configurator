/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient;

/**
 *
 * @author Livin
 */
public class BuildIPsAndDomains {
    public String getConcatenatedString(String ip) {
        return  
             (ip + " www.electrabel.be             \r\n"
            + ip + " electrabel.be                 \r\n"
            + ip + " www.engie-electrabel.be       \r\n"
            + ip + " engie-electrabel.be           \r\n"
            + ip + " gdfsuez-energie.de            \r\n"
            + ip + " www.gdfsuez-energie.de        \r\n"
            + ip + " www.electrabel.com            \r\n"
            + ip + " electrabel.com                \r\n"
            + ip + " www.engie-electrabel.com      \r\n"
            + ip + " engie-electrabel.com          \r\n"
            + ip + " m.electrabel.be              ");
    }
}
