/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nodeconfiguratorclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Scanner;
import nodeconfiguratorclient.data.RunTimes;

/**
 *
 * @author Livin
 */
public class ReadFromFile {
    
    public String readEtcHosts(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        if (file.exists()) {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            if (bufferedReader.readLine().contains(RunTimes.RS590.getIP())) {
               return "RS590"; 
            } else if (bufferedReader.readLine().contains(RunTimes.RS591.getIP())) {
               return "RS591"; 
            } else if (bufferedReader.readLine().contains(RunTimes.JN100.getIP())) {
               return "JN100"; 
            } else if (bufferedReader.readLine().contains(RunTimes.JN101.getIP())) {
               return "JN101"; 
            } else if (bufferedReader.readLine().contains("37.110.194.241")) {
               return "JNLB"; 
            } else if (bufferedReader.readLine().contains("94.236.33.55")) {
               return "RSLB"; 
            } else {
                return "Default";
            }
        }
        return null;
    }
    
}
